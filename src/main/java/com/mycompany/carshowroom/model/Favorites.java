/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carshowroom.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RENT
 */
@Entity
@Table(name = "favorites")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Favorites.findAll", query = "SELECT f FROM Favorites f"),
    @NamedQuery(name = "Favorites.findByIdCar", query = "SELECT f FROM Favorites f WHERE f.favoritesPK.idCar = :idCar"),
    @NamedQuery(name = "Favorites.findByIdClient", query = "SELECT f FROM Favorites f WHERE f.favoritesPK.idClient = :idClient"),
    @NamedQuery(name = "Favorites.findByNumberTrips", query = "SELECT f FROM Favorites f WHERE f.numberTrips = :numberTrips")})
public class Favorites implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FavoritesPK favoritesPK;
    @Column(name = "number_trips")
    private Integer numberTrips;
    @JoinColumn(name = "id_car", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cars cars;
    @JoinColumn(name = "id_client", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Clients clients;

    public Favorites() {
    }

    public Favorites(FavoritesPK favoritesPK) {
        this.favoritesPK = favoritesPK;
    }

    public Favorites(int idCar, int idClient) {
        this.favoritesPK = new FavoritesPK(idCar, idClient);
    }

    public FavoritesPK getFavoritesPK() {
        return favoritesPK;
    }

    public void setFavoritesPK(FavoritesPK favoritesPK) {
        this.favoritesPK = favoritesPK;
    }

    public Integer getNumberTrips() {
        return numberTrips;
    }

    public void setNumberTrips(Integer numberTrips) {
        this.numberTrips = numberTrips;
    }

    public Cars getCars() {
        return cars;
    }

    public void setCars(Cars cars) {
        this.cars = cars;
    }

    public Clients getClients() {
        return clients;
    }

    public void setClients(Clients clients) {
        this.clients = clients;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (favoritesPK != null ? favoritesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Favorites)) {
            return false;
        }
        Favorites other = (Favorites) object;
        if ((this.favoritesPK == null && other.favoritesPK != null) || (this.favoritesPK != null && !this.favoritesPK.equals(other.favoritesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.carshowroom.model.Favorites[ favoritesPK=" + favoritesPK + " ]";
    }
    
}
