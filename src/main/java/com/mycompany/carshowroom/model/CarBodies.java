/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carshowroom.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RENT
 */
@Entity
@Table(name = "car_bodies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CarBodies.findAll", query = "SELECT c FROM CarBodies c"),
    @NamedQuery(name = "CarBodies.findById", query = "SELECT c FROM CarBodies c WHERE c.id = :id"),
    @NamedQuery(name = "CarBodies.findByBody", query = "SELECT c FROM CarBodies c WHERE c.body = :body"),
    @NamedQuery(name = "CarBodies.findByPriceAdd", query = "SELECT c FROM CarBodies c WHERE c.priceAdd = :priceAdd")})
public class CarBodies implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "body")
    private String body;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "price_add")
    private String priceAdd;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBody")
    private Collection<Cars> carsCollection;

    public CarBodies() {
    }

    public CarBodies(Integer id) {
        this.id = id;
    }

    public CarBodies(Integer id, String body, String priceAdd) {
        this.id = id;
        this.body = body;
        this.priceAdd = priceAdd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPriceAdd() {
        return priceAdd;
    }

    public void setPriceAdd(String priceAdd) {
        this.priceAdd = priceAdd;
    }

    @XmlTransient
    public Collection<Cars> getCarsCollection() {
        return carsCollection;
    }

    public void setCarsCollection(Collection<Cars> carsCollection) {
        this.carsCollection = carsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarBodies)) {
            return false;
        }
        CarBodies other = (CarBodies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.carshowroom.model.CarBodies[ id=" + id + " ]";
    }
    
}
