/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carshowroom.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RENT
 */
@Entity
@Table(name = "cars")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cars.findAll", query = "SELECT c FROM Cars c"),
    @NamedQuery(name = "Cars.findById", query = "SELECT c FROM Cars c WHERE c.id = :id"),
    @NamedQuery(name = "Cars.findByName", query = "SELECT c FROM Cars c WHERE c.name = :name"),
    @NamedQuery(name = "Cars.findByType", query = "SELECT c FROM Cars c WHERE c.type = :type"),
    @NamedQuery(name = "Cars.findByPriceBasic", query = "SELECT c FROM Cars c WHERE c.priceBasic = :priceBasic"),
    @NamedQuery(name = "Cars.findByWim", query = "SELECT c FROM Cars c WHERE c.wim = :wim")})
public class Cars implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price_basic")
    private int priceBasic;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WIM")
    private int wim;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cars")
    private Collection<Favorites> favoritesCollection;
    @JoinColumn(name = "id_body", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CarBodies idBody;
    @JoinColumn(name = "id_engine", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Engines idEngine;
    @JoinColumn(name = "id_testtrip", referencedColumnName = "id")
    @ManyToOne
    private Testtrips idTesttrip;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCar")
    private Collection<Agreements> agreementsCollection;

    public Cars() {
    }

    public Cars(Integer id) {
        this.id = id;
    }

    public Cars(Integer id, String name, String type, int priceBasic, int wim) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.priceBasic = priceBasic;
        this.wim = wim;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPriceBasic() {
        return priceBasic;
    }

    public void setPriceBasic(int priceBasic) {
        this.priceBasic = priceBasic;
    }

    public int getWim() {
        return wim;
    }

    public void setWim(int wim) {
        this.wim = wim;
    }

    @XmlTransient
    public Collection<Favorites> getFavoritesCollection() {
        return favoritesCollection;
    }

    public void setFavoritesCollection(Collection<Favorites> favoritesCollection) {
        this.favoritesCollection = favoritesCollection;
    }

    public CarBodies getIdBody() {
        return idBody;
    }

    public void setIdBody(CarBodies idBody) {
        this.idBody = idBody;
    }

    public Engines getIdEngine() {
        return idEngine;
    }

    public void setIdEngine(Engines idEngine) {
        this.idEngine = idEngine;
    }

    public Testtrips getIdTesttrip() {
        return idTesttrip;
    }

    public void setIdTesttrip(Testtrips idTesttrip) {
        this.idTesttrip = idTesttrip;
    }

    @XmlTransient
    public Collection<Agreements> getAgreementsCollection() {
        return agreementsCollection;
    }

    public void setAgreementsCollection(Collection<Agreements> agreementsCollection) {
        this.agreementsCollection = agreementsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cars)) {
            return false;
        }
        Cars other = (Cars) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.carshowroom.model.Cars[ id=" + id + " ]";
    }
    
}
