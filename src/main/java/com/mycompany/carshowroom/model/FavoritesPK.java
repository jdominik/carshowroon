/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carshowroom.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author RENT
 */
@Embeddable
public class FavoritesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_car")
    private int idCar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_client")
    private int idClient;

    public FavoritesPK() {
    }

    public FavoritesPK(int idCar, int idClient) {
        this.idCar = idCar;
        this.idClient = idClient;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCar;
        hash += (int) idClient;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FavoritesPK)) {
            return false;
        }
        FavoritesPK other = (FavoritesPK) object;
        if (this.idCar != other.idCar) {
            return false;
        }
        if (this.idClient != other.idClient) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.carshowroom.model.FavoritesPK[ idCar=" + idCar + ", idClient=" + idClient + " ]";
    }
    
}
