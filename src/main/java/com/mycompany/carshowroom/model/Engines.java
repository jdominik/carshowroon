/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carshowroom.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RENT
 */
@Entity
@Table(name = "engines")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Engines.findAll", query = "SELECT e FROM Engines e"),
    @NamedQuery(name = "Engines.findById", query = "SELECT e FROM Engines e WHERE e.id = :id"),
    @NamedQuery(name = "Engines.findByVendor", query = "SELECT e FROM Engines e WHERE e.vendor = :vendor"),
    @NamedQuery(name = "Engines.findByName", query = "SELECT e FROM Engines e WHERE e.name = :name"),
    @NamedQuery(name = "Engines.findByCapacity", query = "SELECT e FROM Engines e WHERE e.capacity = :capacity"),
    @NamedQuery(name = "Engines.findByPower", query = "SELECT e FROM Engines e WHERE e.power = :power"),
    @NamedQuery(name = "Engines.findByPriceAdd", query = "SELECT e FROM Engines e WHERE e.priceAdd = :priceAdd")})
public class Engines implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "vendor")
    private String vendor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "capacity")
    private String capacity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "power")
    private int power;
    @Size(max = 45)
    @Column(name = "price_add")
    private String priceAdd;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEngine")
    private Collection<Cars> carsCollection;

    public Engines() {
    }

    public Engines(Integer id) {
        this.id = id;
    }

    public Engines(Integer id, String vendor, String name, String capacity, int power) {
        this.id = id;
        this.vendor = vendor;
        this.name = name;
        this.capacity = capacity;
        this.power = power;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getPriceAdd() {
        return priceAdd;
    }

    public void setPriceAdd(String priceAdd) {
        this.priceAdd = priceAdd;
    }

    @XmlTransient
    public Collection<Cars> getCarsCollection() {
        return carsCollection;
    }

    public void setCarsCollection(Collection<Cars> carsCollection) {
        this.carsCollection = carsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Engines)) {
            return false;
        }
        Engines other = (Engines) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.carshowroom.model.Engines[ id=" + id + " ]";
    }
    
}
