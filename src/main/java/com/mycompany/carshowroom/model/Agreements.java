/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carshowroom.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RENT
 */
@Entity
@Table(name = "agreements")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agreements.findAll", query = "SELECT a FROM Agreements a"),
    @NamedQuery(name = "Agreements.findById", query = "SELECT a FROM Agreements a WHERE a.id = :id"),
    @NamedQuery(name = "Agreements.findByIdClient", query = "SELECT a FROM Agreements a WHERE a.idClient = :idClient"),
    @NamedQuery(name = "Agreements.findByPrice", query = "SELECT a FROM Agreements a WHERE a.price = :price")})
public class Agreements implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_client")
    private int idClient;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private int price;
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Clients clients;
    @JoinColumn(name = "id_car", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cars idCar;

    public Agreements() {
    }

    public Agreements(Integer id) {
        this.id = id;
    }

    public Agreements(Integer id, int idClient, int price) {
        this.id = id;
        this.idClient = idClient;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Clients getClients() {
        return clients;
    }

    public void setClients(Clients clients) {
        this.clients = clients;
    }

    public Cars getIdCar() {
        return idCar;
    }

    public void setIdCar(Cars idCar) {
        this.idCar = idCar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agreements)) {
            return false;
        }
        Agreements other = (Agreements) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.carshowroom.model.Agreements[ id=" + id + " ]";
    }
    
}
